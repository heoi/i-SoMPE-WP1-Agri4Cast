cat("Saving output...\n")

# Create subfolder with reformatted date and time.
## Overwrite daily outputs
path <- file.path(odir, format(Sys.time(), "%Y%m%d"))
## Keep every output
#path <- file.path(odir, format(Sys.time(), "%Y%m%dT%H%M%SZ%z"))

dir.create(path, showWarnings = FALSE)

# Your code goes here

# save table
write.csv(A4C_per_grid,paste(path,"/A4C_per_grid.csv",sep = ''), row.names = FALSE)

# save geometry files
shape <- read_sf(dsn=path_A4C_shp_data)
colnames(shape) <- c("IDGRID","elevation","Area_grid","geometry")
shape <- left_join(shape,A4C_per_grid)
shape <- filter(shape, !(is.na(meanTemp))) %>% # filter out lines with no values
         relocate(geometry, .after = share_CC_H2O_limit)
st_write(shape, paste(path,"/A4C_per_grid_geometry_",format(Sys.time(), "%Y%m%d"),".gpkg",sep = ''))

# save raster files
  #create generic raster
  raster <- raster::raster(nrows = 208, ncols = 241)
  crs(raster) <- crs(shape)
  extent(raster) <- extent(shape)
  
  #create raster files
  names_col <- names(shape)
  names_col <- names_col[!(names_col %in% c("IDGRID","elevation","Area_grid","GrowSeason_class","TempSum_class","geometry"))]
  stack_raster <- stack(raster) # initialize raster stack
  
  for (i in 1:length(names_col)) {
      
      raster_file <- raster::rasterize(shape, raster, field=names_col[i], background = -99999)
      writeRaster(raster_file, paste(path,"/",names_col[i],sep = ''), format = "GTiff", overwrite = 1)
  
      #Add to raster stack
      raster_file <- setNames(raster_file, names_col[i])
      stack_raster[[i]] <- raster_file
      rm(raster_file)
  }


# Purge obsolete variables
rm(path, raster, names_col)

cat("Output saved!\n")
