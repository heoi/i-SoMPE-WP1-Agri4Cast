[//]: # (Title: i-SoMPE WP1 Agri4Cast R Project)  
[//]: # (Author: Olivier Heller)  
[//]: # (Date: April 20, 2022)  

# i-SoMPE WP1 Agri4Cast R Project

## About this R Project
This R projcet was written for the [i-SoMPE](https://isompe.gitlab.io/blog/) project within EJP SOIL in 2021 and 2022 by [Olivier Heller](mailto:olivier.heller@agroscope.admin.ch). It was used to calculate various climatic data for the characterization of the Agro-Environmental Zones (AEZ) of Europe and to assess the potential area of application of soil management practices.

## How to use this Project
### Input data
The input data (Agri4Cast long term averages) to the calculations need to be available to the user. The file path to the input data needs to be customized in the file "00-LoadPath.R". The input data must contain long term average (1991 - 2020) climatic data (min. temp., max. temp., mean. temp., ET0, Prec.) for every day of the year for every Agri4Cast grid cell. The input data is available upon request from the [Agri4Cast Website](https://agri4cast.jrc.ec.europa.eu/DataPortal/Index.aspx) or upon request from the [author](mailto:olivier.heller@agroscope.admin.ch).


### Output
The code calculates 11 climatic variables for every grid cell:

- *meanTemp*: Mean annual temperature
- *meanPrec*: Mean annual precipitation sum 
- *meanET0*: Mean annual potential evapotranspiration sum
- *GrowSeason*: Average length of the growing season (number of days per year with average temperature above 5°C)
- *GrowSeason_class*: Classification of the length of the growing season: < 200 days – short; 200 to 300 days – intermediate; >300 days – long ([Metzger et al., 2012](https://edepot.wur.nl/197197))
- *TempSum* :Average annual temperature sum (sum of all average daily temperatures above 0°C)
- *TempSum_class*: Classification of the temperature sum: < 2000 °C = cold; 2000 to 3000 °C = cool temperate; 3000 to 4000 °C = temperate; 4000 to 5000 °C = warm temperate; > 5000 °C = hot ([Metzger et al., 2012](https://edepot.wur.nl/197197))
- *HeatDay*: Average number of heat days (number of days per year with average maximal temperature above 30°C)
- *FrostDay*: Average number of frost days (number of days per year with average minimal temperature below 0°C)
- *WaterBalance*: Mean annual water balance (meanPrec – meanET0)
- *DryDay*: Average number of days per year with a negative water balance (Prec – ET0)


### Optional calculations of cover crop suitability
The code includes a script that calculates the climatic suitability for cover crop growth, based on a few simple assumptions. By default the calculations are not activated. If needed, please activate the sourcing of "07-TransformData_Cover_Crop.R" on line 35 of the "00-Main.R" file.

#### Assumptions for cover crop suitabilty

The calculations are based on the following assumptions:

- Cover crops are grown after winter wheat
- Winter wheat needs a specific temperatur sum to flowering and to maturity. The applied model was described by [Olesen et al. (2012)](https://doi.org/10.1080/19440049.2012.712060)
- Cover crops need daily average temperature >= 4°C and a minimum daily 31-day-average-precipitation of at least 50% of the daily potential evapotranspiration (ET0)
  
### Run the project

1. Request LTA 1991-2020 data from [Agri4Cast](https://agri4cast.jrc.ec.europa.eu/DataPortal/Index.aspx)
2. Download Agri4cast grid shape file from [Agri4Cast](https://agri4cast.jrc.ec.europa.eu/DataPortal/Resource_Files/SupportFiles/grid25.zip)
3. Adjust paths to data in the "00-Main.R" file
4. Run "00-Main.R"
5. The output will be found in the "output" folder
