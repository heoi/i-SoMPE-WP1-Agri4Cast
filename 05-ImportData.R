cat("Importing data...\n")

# Your code goes here
Agri4cast <- read.csv(path_A4C_org_data, sep=";")

# Purge obsolete variables
rm()

cat("Data imported!\n")
